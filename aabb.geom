#version 150

uniform mat4 ModelViewProjectionMatrix;

layout(points) in;
layout(line_strip, max_vertices = 16) out;

in vert_geom
{
    vec4 min;
    vec4 max;
}_in[];

void main()
{
    vec4 v[8];
    vec4 min = _in[0].min;
    vec4 max = _in[0].max;

    for(int i=0;i<8;i++)v[i] = min;
    v[1].x = max.x;
    v[2].xz = max.xz;
    v[3].z = max.z;
    v[4].y = max.y;
    v[5].xy = max.xy;
    v[6] = max;
    v[7].yz = max.yz;

    gl_Position = ModelViewProjectionMatrix * v[0];
    EmitVertex();
    gl_Position = ModelViewProjectionMatrix * v[1];
    EmitVertex();
    gl_Position = ModelViewProjectionMatrix * v[2];
    EmitVertex();
    gl_Position = ModelViewProjectionMatrix * v[3];
    EmitVertex();
    gl_Position = ModelViewProjectionMatrix * v[0];
    EmitVertex();
    gl_Position = ModelViewProjectionMatrix * v[4];
    EmitVertex();
    gl_Position = ModelViewProjectionMatrix * v[5];
    EmitVertex();
    gl_Position = ModelViewProjectionMatrix * v[6];
    EmitVertex();
    gl_Position = ModelViewProjectionMatrix * v[7];
    EmitVertex();
    gl_Position = ModelViewProjectionMatrix * v[4];
    EmitVertex();
    EndPrimitive();

    gl_Position = ModelViewProjectionMatrix * v[1];
    EmitVertex();
    gl_Position = ModelViewProjectionMatrix * v[5];
    EmitVertex();
    EndPrimitive();

    gl_Position = ModelViewProjectionMatrix * v[2];
    EmitVertex();
    gl_Position = ModelViewProjectionMatrix * v[6];
    EmitVertex();
    EndPrimitive();

    gl_Position = ModelViewProjectionMatrix * v[3];
    EmitVertex();
    gl_Position = ModelViewProjectionMatrix * v[7];
    EmitVertex();
    EndPrimitive();
}
