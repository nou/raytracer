#include "glwidget.h"

#include <iostream>
#include <cmath>
#include <CL/cl.hpp>
#include "tracer.h"
#include <QTimer>
#include "objmodel.h"

GLWidget::GLWidget(QGLFormat &format) : QGLWidget(format),
    start(0),
    count(1)
{
    resize(512, 512);
}

GLWidget::~GLWidget()
{
}

void GLWidget::initializeGL()
{
    f = context()->contextHandle()->versionFunctions<QOpenGLFunctions_3_2_Core>();

    GLuint vao;
    f->glGenVertexArrays(1, &vao);
    f->glBindVertexArray(vao);

    qDebug() << (char*)glGetString(GL_VERSION);

    program = new QOpenGLShaderProgram(this);
    program->addShaderFromSourceFile(QOpenGLShader::Vertex, "shader.vert");
    program->addShaderFromSourceFile(QOpenGLShader::Fragment, "shader.frag");
    program->bindAttributeLocation("vertex", 0);
    program->link();

    AssImpModel model("mesh.obj");
    auto vertex = model.getVertexs();
    auto faces = model.getFaces();
    int indx[faces.size()*3];
    for(uint i=0;i<faces.size();i++)
    {
        indx[i*3] = faces[i].a;
        indx[i*3+1] = faces[i].b;
        indx[i*3+2] = faces[i].c;
    }

    vt.create();
    vt.bind();
    vt.allocate(&vertex[0], sizeof(aiVector3D)*vertex.size());

    indices = QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);
    indices.create();
    indices.bind();
    indices.allocate(indx, sizeof(indx));

    obj_count = faces.size()*3;

    QMatrix4x4 modelview;
    QMatrix4x4 projection;
    projection.perspective(50, 1, 0.1, 10000);
    modelview.lookAt(QVector3D(0, 3, 3), QVector3D(0, 0, 0), QVector3D(0, 1, 0));

    program->bind();
    mat = projection*modelview;
    program->setUniformValue("ModelViewProjectionMatrix", mat);

    QTimer *timer = new QTimer(this);
    timer->setInterval(20);
    connect(timer, SIGNAL(timeout()), this, SLOT(rotate()));
    timer->start();
    glEnable(GL_DEPTH_TEST);

    aabb = new QOpenGLShaderProgram(this);
    aabb->addShaderFromSourceFile(QOpenGLShader::Vertex, "aabb.vert");
    aabb->addShaderFromSourceFile(QOpenGLShader::Geometry, "aabb.geom");
    aabb->addShaderFromSourceFile(QOpenGLShader::Fragment, "aabb.frag");
    aabb->bindAttributeLocation("min", 0);
    aabb->bindAttributeLocation("max", 1);
    aabb->link();
    aabb->bind();
    aabb->setUniformValue("ModelViewProjectionMatrix", mat);

    tree = BVH::Tree(model.getFaces(), model.getVertexs());
}

void GLWidget::resizeGL(int w, int h)
{
    f->glViewport(0, 0, w, h);
}

void GLWidget::paintGL()
{
    f->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    program->bind();
    program->setUniformValue("ModelViewProjectionMatrix", mat);
    f->glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    vt.bind();
    f->glVertexAttribPointer(0, 3, GL_FLOAT, 0, sizeof(aiVector3D), 0);
    f->glEnableVertexAttribArray(0);
    f->glDisableVertexAttribArray(1);
    indices.bind();
    f->glDrawElements(GL_TRIANGLES, obj_count, GL_UNSIGNED_INT, 0);


    vertex.bind();
    f->glEnableVertexAttribArray(0);
    f->glEnableVertexAttribArray(1);
    f->glVertexAttribPointer(0, 3, GL_FLOAT, false, sizeof(BVH::LinearNode), 0);
    f->glVertexAttribPointer(1, 3, GL_FLOAT, false, sizeof(BVH::LinearNode), (void*)(sizeof(cl_float)*3));
    aabb->bind();
    aabb->setUniformValue("ModelViewProjectionMatrix", mat);
    aabb->setUniformValue("color", QVector4D(0, 1, 0, 1));
    f->glDrawArrays(GL_POINTS, start, count>3000 ? 3000 : count);

    aabb->setUniformValue("color", QVector4D(1, 1, 0, 1));
    f->glDrawArrays(GL_POINTS, start*2+1, count*2);
}

void GLWidget::keyPressEvent(QKeyEvent *event)
{
    if(event->key()==Qt::Key_Up)
    {
        start = 2*start+1;
        count *= 2;
    }
    if(event->key()==Qt::Key_Down)
    {
        start = (start-1)/2;
        count /= 2;
    }
    if(count<=0)count = 1;
    //if(count>3*1024)count = 3*1024;

    if(event->key()==Qt::Key_Right)
    {
        mat.rotate(0.5, 0, 1, 0);
        program->setUniformValue("ModelViewProjectionMatrix", mat);
    }
    if(event->key()==Qt::Key_Left)
    {
        mat.rotate(-0.5, 0, 1, 0);
        program->setUniformValue("ModelViewProjectionMatrix", mat);
    }
    if(event->key()==Qt::Key_O)glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    if(event->key()==Qt::Key_P)glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    //updateGL();
}

void GLWidget::rotate()
{
    mat.rotate(-0.5, 0, 1, 0);
    updateGL();
}
