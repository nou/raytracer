#ifndef IMGVIEW_H
#define IMGVIEW_H

#include <QLabel>

class ImgView : public QLabel
{
    Q_OBJECT
    QList<QImage> frames;
    int current;
public:
    explicit ImgView(QWidget *parent = 0);
    void setFrames(QList<QImage> _frames);
    int numFrames();
    void saveFrames(const QString &path);
public slots:
    void nextFrame();
    void prevFrame();
    void showFrame(int frame);
};

#endif // IMGVIEW_H
