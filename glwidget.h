#ifndef _GLWIDGET_H_NOU_
#define _GLWIDGET_H_NOU_

#include <GL/gl.h>
#define GL_GLEXT_PROTOTYPES 1
#include <GL/glext.h>
#include <QKeyEvent>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QGLFormat>
#include <QGLWidget>
#include <QOpenGLFunctions_3_2_Core>
#include "bvh/tree.h"

using namespace std;

class GLWidget : public QGLWidget
{
    Q_OBJECT
    int start, count;
    QOpenGLShaderProgram *program, *aabb;
    QOpenGLBuffer vertex, vt, indices;
    QOpenGLFunctions_3_2_Core *f;
    int obj_count;
    QMatrix4x4 mat;
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();
    BVH::Tree tree;
    public:
    GLWidget(QGLFormat &format);
    ~GLWidget();
    void keyPressEvent(QKeyEvent *event);
public slots:
    void rotate();
};

#endif // _GLWIDGET_H_NOU_
