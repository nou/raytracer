#include "imgview.h"
#include <QKeyEvent>
#include <QMessageBox>
#include "tracerconfig.h"

ImgView::ImgView(QWidget *parent) : QLabel(parent)
{
}

void ImgView::setFrames(QList<QImage> _frames)
{
    frames = _frames;
    current = 0;
    if(frames.size())
    {
        showFrame(0);
    }
}

int ImgView::numFrames()
{
    return frames.size();
}

void ImgView::saveFrames(const QString &path)
{
    int i = 0;
    foreach(const QImage &frame, frames)
    {
        frame.save(QString("%1/%2.png").arg(path).arg(i++, 4, 10, QChar('0')));
    }
}

void ImgView::nextFrame()
{
    if(current<frames.size()-1)
    {
        current++;
        showFrame(current);
    }
}

void ImgView::prevFrame()
{
    if(current>0)
    {
        current--;
        showFrame(current);
    }
}

void ImgView::showFrame(int frame)
{
    if(frame>=0 && frame<frames.size())
    {
        setPixmap(QPixmap::fromImage(frames.at(frame)));
        current = frame;
    }
}
