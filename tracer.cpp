#include "tracer.h"
#include <QImage>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <QLabel>
#include <QThread>
#include <QFile>
#include <QElapsedTimer>
#include <QSettings>
#include <random>
#include "mwc64x.h"

typedef struct
{
    cl_float3 intesity;
    cl_float3 position;
}Light;

typedef struct
{
    cl_float3 origin;
    cl_float3 dir;
}Ray;

cl_float4 cross(cl_float3 a, cl_float3 b)
{
    cl_float4 ret;
    ret.x = a.y*b.z - a.z*b.y;
    ret.y = a.z*b.x - a.x*b.z;
    ret.z = a.x*b.y - a.y*b.x;
    ret.w = 0;
    return ret;
}

cl_float3 vec_minus(cl_float3 a, cl_float3 b)
{
    return {a.x-b.x, a.y-b.y, a.z-b.z};
}

cl_float4 normalize(cl_float4 a)
{
    float l = 1.0f/sqrt(a.x*a.x + a.y*a.y + a.z*a.z + a.w*a.w);
    return {a.x*l, a.y*l, a.z*l, a.w*l};
}

cl_float16 lookAt(cl_float3 eye, cl_float3 center, cl_float3 up)
{
    cl_float16 ret;
    cl_float3 dir = vec_minus(center, eye);
    ret.lo.lo = normalize(cross(dir, up));
    ret.lo.hi = normalize(cross(ret.lo.lo, dir));
    ret.hi.lo = normalize(dir);
    ret.hi.hi = eye;
    ret.hi.hi.w = 1;

    return ret;
}

void Tracer::initPRNG(uint numStates)
{
    cl_ulong *seed = new cl_ulong[numStates];

    std::mt19937_64 rnd;
    std::uniform_int_distribution<> dist;

    for(uint i=0;i<numStates;i++)
    {
        seed[i] = rnd();
        if(seed[i]==0)seed[i]++;
    }

    buffer_prng = cl::Buffer(*context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, numStates*sizeof(cl_uint2), seed);
    delete [] seed;
}

cl_uint Tracer::imgLength() const
{
    return config.imgSize.x*config.imgSize.y;
}

cl_uint Tracer::framesLenght() const
{
    return imgLength()*config.numFrames*3;
}

void Tracer::integrate(const cl_float4 *mappedBuffer)
{
    const float timeScale = 1.0f/config.frameStep;

    const uint len = imgLength();

    #pragma omp parallel for
    for(uint i=0;i<len;i++)
    {
        for(int o=0;o<config.traceDepth;o++)
        {
            cl_float4 sample = mappedBuffer[len*o+i];
            if(sample.w<0)break;

            if(sample.x>0 || sample.y>0 || sample.z>0)
                minDistance[i] = min(minDistance[i], sample.w);

            int t = min((int)(sample.w*timeScale), config.numFrames-1);
            frames[t*len*3 + i*3]   += sample.x;
            frames[t*len*3 + i*3+1] += sample.y;
            frames[t*len*3 + i*3+2] += sample.z;
        }
    }
}

void Tracer::initBuffers()
{
    buffer_vertexs = cl::Buffer(*context, CL_MEM_READ_ONLY, mesh->getVertexs().size()*sizeof(aiVector3D), nullptr);
    buffer_nodes = cl::Buffer(*context, CL_MEM_READ_ONLY, tree->linearTree.size()*sizeof(BVH::LinearNode), nullptr);
    buffer_materials = cl::Buffer(*context, CL_MEM_READ_ONLY, mesh->getMaterials().size()*sizeof(Material), nullptr);
    buffer_samples[0] = cl::Buffer(*context, CL_MEM_READ_WRITE, imgLength()*sizeof(cl_float4)*config.traceDepth);
    buffer_samples[1] = cl::Buffer(*context, CL_MEM_READ_WRITE, imgLength()*sizeof(cl_float4)*config.traceDepth);
    buffer_rays = cl::Buffer(*context, CL_MEM_READ_WRITE, imgLength()*sizeof(Ray));

    queue->enqueueWriteBuffer(buffer_vertexs, false, 0,  mesh->getVertexs().size()*sizeof(aiVector3D), &mesh->getVertexs()[0]);
    queue->enqueueWriteBuffer(buffer_nodes, false, 0,  tree->linearTree.size()*sizeof(BVH::LinearNode), &tree->linearTree[0]);
    queue->enqueueWriteBuffer(buffer_materials, false, 0,  mesh->getMaterials().size()*sizeof(Material), &mesh->getMaterials()[0]);

    cl_float3 pattern = {0, 0, 0, 0};
    queue->enqueueFillBuffer(buffer_samples[0], pattern, 0, imgLength()*sizeof(cl_float4)*config.traceDepth);
    queue->enqueueFillBuffer(buffer_samples[1], pattern, 0, imgLength()*sizeof(cl_float4)*config.traceDepth);

    initPRNG(imgLength());

    cl::ImageFormat format(CL_BGRA, CL_UNORM_INT8);
    tex = unique_ptr<cl::Image2D>(new cl::Image2D(*context, CL_MEM_WRITE_ONLY, format, config.imgSize.x, config.imgSize.y));

    cl_float16 viewMat = lookAt(config.cameraPos, config.cameraCenter, {0, 1, 0});
    init_rays->setArg(0, buffer_rays);
    init_rays->setArg(1, buffer_prng);
    init_rays->setArg(2, viewMat);
}

// 0.0 - 6.0 to RGB
cl_int3 RGBramp(float v)
{
    cl_int3 ret = {0, 0, 0};
    if(v<1)
    {
        ret.x = 255;
        ret.y = 255*v;
        ret.z = 0;
    }
    else if(v<2)
    {
        ret.x = (2-v)*255;
        ret.y = 255;
        ret.z = 0;
    }
    else if(v<3)
    {
        ret.x = 0;
        ret.y = 255;
        ret.z = (v-2)*255;
    }
    else if(v<4)
    {
        ret.x = 0;
        ret.y = (4-v)*255;
        ret.z = 255;
    }
    else if(v<5)
    {
        ret.x = (v-4)*255;
        ret.y = 0;
        ret.z = 255;
    }
    else if(v<6)
    {
        ret.x = 255;
        ret.y = 0;
        ret.z = (6-v)*255;
    }
    return ret;
}

QImage Tracer::getMinDistance() const
{
    QImage ret(config.imgSize.x, config.imgSize.y, QImage::Format_RGB32);

    uchar *data = ret.bits();
    const uint len = imgLength();
    float maxDist = 0;
    float minDist = INFINITY;

    for(uint i=0;i<len;i++)
    {
        if(isfinite(minDistance[i]))
            maxDist = max(minDistance[i], maxDist);
        minDist = min(minDistance[i], minDist);
    }

    maxDist = 6.0f/(maxDist-minDist);

    for(uint i=0;i<len;i++)
    {
        cl_int3 c = RGBramp((minDistance[i]-minDist)*maxDist);
        data[i*4+0] = c.x;
        data[i*4+1] = c.y;
        data[i*4+2] = c.z;
    }

    return ret;
}

Tracer::Tracer() :
    numSamples(0),
    frames(0),
    minDistance(0)
{
}

Tracer::~Tracer()
{
    delete [] frames;
    delete [] minDistance;
}

void Tracer::initWithDevice(cl::Device &device)
{
    cl::Platform platform;
    device.getInfo(CL_DEVICE_PLATFORM, &platform);
    string name;

    platform.getInfo(CL_PLATFORM_NAME, &name);
    cout << "Platform name: " << name << endl;
    cout << "Device name: " << device.getInfo<CL_DEVICE_NAME>() << endl;

    cl_context_properties properties[] = {  CL_CONTEXT_PLATFORM, (cl_context_properties)(platform()), 0 };
    vector<cl::Device> devices;
    devices.push_back(device);
    context = unique_ptr<cl::Context>(new cl::Context(devices, properties, 0, 0));

    QSettings settings;
    QByteArray binary = settings.value("opencl/binary").toByteArray();
    if(!binary.isEmpty())
    {
        try
        {
            cl::Program::Binaries binaries;
            binaries.push_back(pair<const void*, ::size_t>(binary.constData(), binary.size()));
            vector<cl::Device> devices = {device};
            program = unique_ptr<cl::Program>(new cl::Program(*context, devices, binaries));
            program->build();
        }
        catch(cl::Error err)
        {
            program.reset();
        }
    }

    if(!program)
    {
        QFile fr(":/intersect_kernel.cl");
        fr.open(QIODevice::ReadOnly);
        string source = QString::fromUtf8(fr.readAll()).toStdString();

        try
        {
            program = unique_ptr<cl::Program>(new cl::Program(*context, source, false));
            cl_int err = program->build();
            if(err!=CL_SUCCESS)
            {
                string log;
                program->getBuildInfo(device, CL_PROGRAM_BUILD_LOG, &log);
                cout << log << endl;
            }
        }
        catch(cl::Error err)
        {
            if(err.err()==CL_BUILD_PROGRAM_FAILURE)
            {
                string log;
                program->getBuildInfo(device, CL_PROGRAM_BUILD_LOG, &log);
                cout << log << endl;
            }
            throw err;
        }
        vector<size_t> sizes;
        vector<char*> binaries;
        program->getInfo(CL_PROGRAM_BINARY_SIZES, &sizes);
        binaries.push_back(new char[sizes[0]]);
        program->getInfo(CL_PROGRAM_BINARIES, &binaries);
        settings.setValue("opencl/binary", QByteArray(binaries[0], sizes[0]));
    }


    aabb_kernel = unique_ptr<cl::Kernel>(new cl::Kernel(*program, "aabb_traverse"));
    get_image_kernel = unique_ptr<cl::Kernel>(new cl::Kernel(*program, "get_image"));
    init_rays = unique_ptr<cl::Kernel>(new cl::Kernel(*program, "init_rays"));

    queue = unique_ptr<cl::CommandQueue>(new cl::CommandQueue(*context, device));
}

void Tracer::initWithSceneConfig(TracerConfig &_config)
{
    if(!_config.valid)
        return;

    config = _config;

    numSamples = 0;

    mesh = unique_ptr<AssImpModel>(new AssImpModel(config.model));
    tree = unique_ptr<BVH::Tree>(new BVH::Tree(mesh->getFaces(), mesh->getVertexs()));

    initBuffers();

    delete [] frames;
    delete [] minDistance;

    frames = new float[framesLenght()];
    minDistance = new float[imgLength()];
    memset(frames, 0, framesLenght()*sizeof(float));
    for(uint i=0;i<imgLength();i++)
        minDistance[i] = INFINITY;
}

cl::Event Tracer::traceStep()
{
    cl_uint2 numNodes = {(cl_uint)tree->linearTree.size(), (cl_uint)config.traceDepth};
    Light light;
    light.intesity = config.lightColor;
    light.position = config.lightPos;
    aabb_kernel->setArg(0, numNodes);
    aabb_kernel->setArg(1, buffer_nodes);
    aabb_kernel->setArg(2, buffer_vertexs);
    aabb_kernel->setArg(3, buffer_rays);
    aabb_kernel->setArg(4, buffer_samples[numSamples&1]);
    aabb_kernel->setArg(5, buffer_prng);
    aabb_kernel->setArg(6, light);
    aabb_kernel->setArg(7, buffer_materials);

    cl::Event ret;

    queue->enqueueNDRangeKernel(*init_rays, cl::NullRange, cl::NDRange(config.imgSize.x, config.imgSize.y), cl::NullRange);
    queue->enqueueNDRangeKernel(*aabb_kernel, cl::NullRange, cl::NDRange(config.imgSize.x, config.imgSize.y), cl::NullRange, nullptr, &ret);
    queue->flush();

    numSamples++;
    cout << "Kreslim " << numSamples << endl;
    return ret;
}

void Tracer::integrateStepFirst()
{

    mappedIdx = (numSamples+1)&1;
    mapped = static_cast<cl_float4*>(queue->enqueueMapBuffer(buffer_samples[mappedIdx], CL_TRUE, CL_MAP_READ, 0,
                                                             imgLength()*sizeof(cl_float4)*config.traceDepth));
}

void Tracer::integrateStepSecond()
{
    integrate(mapped);
    queue->enqueueUnmapMemObject(buffer_samples[mappedIdx], mapped);
    mapped = 0;
}

uint Tracer::getNumSamples() const
{
    return numSamples;
}

inline uchar sRGB(float r, float scale)
{
    int n = (r>0.0031308f ? 1.055f*pow(r, 1/2.4f)-0.055f : 12.92f*r)*scale;
    n &= -(n >= 0);
    return n | ((255 - n) >> 31);
}

QList<QImage> Tracer::retriveFrames(float gain) const
{
    QVector<QImage> ret;
    const uint len = imgLength();
    const float invSamples = 1.0f/numSamples;
    const float scale = 256.0f*gain;

    for(int i=0;i<config.numFrames;i++)
        ret.push_back(QImage(config.imgSize.x, config.imgSize.y, QImage::Format_RGB32));

    #pragma omp parallel for
    for(int i=0;i<config.numFrames;i++)
    {
        uchar *data = ret[i].bits();
        const float *frame = frames+i*len*3;
        for(uint o=0;o<len;o++)
        {
            data[o*4+0] = sRGB(frame[o*3+2]*invSamples, scale);
            data[o*4+1] = sRGB(frame[o*3+1]*invSamples, scale);
            data[o*4+2] = sRGB(frame[o*3]*invSamples, scale);
            data[o*4+3] = 255;
        }
    }

    ret.push_back(getMinDistance());

    return ret.toList();
}

bool Tracer::saveFrames(const QString &path)
{
    QFile fw(path);
    QDataStream stream(&fw);
    if(fw.open(QIODevice::WriteOnly))
    {
        stream << numSamples << config.imgSize.x << config.imgSize.y << config.numFrames << config.frameStep;
        stream.writeRawData((char*)frames, framesLenght()*sizeof(float));
        return true;
    }
    return false;
}

bool Tracer::loadFrames(const QString &path)
{
    QFile fr(path);
    QDataStream stream(&fr);
    if(fr.open(QIODevice::ReadOnly))
    {
        cl_uint _numSamples;
        cl_int _numFrames;
        cl_uint2 size;
        float frameStep;
        stream >> _numSamples >> size.x >> size.y >> _numFrames >> frameStep;
        if(size.x != config.imgSize.x || size.y != config.imgSize.y ||
                _numFrames != config.numFrames || frameStep != config.frameStep)
        {
            return false;
        }
        numSamples = _numSamples;
        stream.readRawData((char*)frames, framesLenght()*sizeof(float));
        return true;
    }
    return false;
}

TracerThread::TracerThread(cl::Device &device)
{
    quit = false;
    tracer = std::unique_ptr<Tracer>(new Tracer);
    tracer->initWithDevice(device);
    thread = new QThread();
    thread->setObjectName("TracerThread");
    this->moveToThread(thread);
    connect(thread, SIGNAL(started()), this, SLOT(run()));
    connect(thread, SIGNAL(finished()), this, SIGNAL(finished()));
}

TracerThread::~TracerThread()
{
    stop();
    thread->wait();
    delete thread;
}

void TracerThread::start(uint numSamples)
{
    mutex.lock();
    samplesToRun = numSamples;
    quit = false;
    mutex.unlock();
    thread->start();
}

void TracerThread::stop()
{
    quit = true;
}

QList<QImage> TracerThread::getFrames(float gain)
{
    return tracer->retriveFrames(gain);
}

bool TracerThread::setScene(TracerConfig &config)
{
    if(!thread->isRunning())
    {
        tracer->initWithSceneConfig(config);
        return true;
    }
    return false;
}

bool TracerThread::isRuning() const
{
    return thread->isRunning();
}

bool TracerThread::saveFrames(const QString &path)
{
    if(!thread->isRunning())
    {
        return tracer->saveFrames(path);
    }
    return false;
}

bool TracerThread::loadFrames(const QString &path)
{
    if(!thread->isRunning())
    {
        bool ret = tracer->loadFrames(path);
        emit sampleFinished(tracer->getNumSamples());
        return ret;
    }
    return false;
}

void TracerThread::setOutputPathGain(const QString &path, float gain)
{
    outputPath = path;
    outputGain = gain;
}

void TracerThread::run()
{
    QElapsedTimer timer;
    tracer->traceStep();
    samplesToRun--;
    while(!quit && samplesToRun>0)
    {
        timer.start();
        tracer->integrateStepFirst();
        tracer->traceStep();
        tracer->integrateStepSecond();
        cout << timer.elapsed() << endl;
        samplesToRun--;
        emit sampleFinished(tracer->getNumSamples());
    }
    emit sampleFinished(tracer->getNumSamples());
    tracer->integrateStepFirst();
    tracer->integrateStepSecond();

    if(!outputPath.isEmpty())
    {
        QList<QImage> frames = getFrames(outputGain);
        int i = 0;
        foreach(const QImage &frame, frames)
        {
            frame.save(QString("%1/%2.png").arg(outputPath).arg(i++, 4, 10, QChar('0')));
        }
    }

    thread->quit();
}
