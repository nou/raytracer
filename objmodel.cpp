#include "objmodel.h"
#include <fstream>
#include <iostream>
#include <sstream>

using namespace std;

ObjModel::ObjModel(const char* filename)
{
    ifstream fr(filename);
    cl_float3 v;
    cl_uint3 f;
    string line;
    if(fr.good())
    {
        while(1)
        {
            getline(fr, line);
            if(fr.eof() || !fr.good())break;

            if(line.substr(0, 2) =="v ")//vertex
            {
                istringstream s(line.substr(2));
                s >> v.x >> v.y >> v.z;
                vertexs.push_back(v);
            }
            else if(line.substr(0, 2)=="f ")//face
            {
                istringstream s(line.substr(2));
                s >> f.x >> f.y >> f.z;
                f.x--; f.y--; f.z--;
                faces.push_back(f);
            }
        }
    }
}

ObjModel::~ObjModel()
{
    //dtor
}
