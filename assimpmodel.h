#ifndef ASSIMPMODEL_H
#define ASSIMPMODEL_H

#include <QString>
#include <CL/cl_platform.h>
#include <assimp/types.h>

struct Face
{
    uint a,b,c,m;
    Face(uint *face, uint mat, uint offset = 0);
};

struct Material
{
    cl_float3 color;
};

class AssImpModel
{
    uint numVertices, numFaces;
    std::vector<aiVector3D> vertexs;
    std::vector<Face> faces;
    std::vector<Material> materials;
public:
    AssImpModel(const QString &path);
    const std::vector<aiVector3D>& getVertexs() const;
    const std::vector<Face>& getFaces() const;
    const std::vector<Material>& getMaterials() const;
};

#endif // ASSIMPMODEL_H
