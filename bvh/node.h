#ifndef NODE_H
#define NODE_H

#include <CL/cl.h>
#include "assimpmodel.h"

namespace BVH
{

const float aabb_epsilon = 0.000001f;

class Node
{
    cl_float3 min, max;
    cl_uint left, right, parent;
public:
    Node();
    Node(uint p);
    Node(uint a, uint b);
    void setLeftNode(uint i);
    void setRightNode(uint i);
    void setParent(uint i);
    void setLeftTriangle(cl_uint i);
    void setRightTriangle(cl_uint i);
    uint getLeft() const { return left&0x7fffffff; }
    uint getRight() const { return right&0x7fffffff; }
    uint getParent() const { return parent; }
    cl_float3 getMin() const { return min; }
    cl_float3 getMax() const { return max; }
    bool isLeftLeaf() const { return left&0x80000000; }
    bool isRightLeaf() const { return right&0x80000000; }
    void calcAABB(const std::vector<Face> &faces, const std::vector<aiVector3D> &vertexs, std::vector<Node> nodes);
};

struct LinearNode
{
    union
    {
        struct
        {
            cl_float min[3];
            cl_float max[3];
        }bvhNode;
        struct
        {
            cl_uint vertexs[3];
            cl_uint matIndex;
        }triangle;
    };
    cl_uint nextNode;

    explicit LinearNode(const Node &node);
    explicit LinearNode(const Face &face);
    cl_float3 getMin() const;
    cl_float3 getMax() const;
    void setNextNode(cl_uint node);
};

}

#endif // NODE_H
