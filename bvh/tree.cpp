#include "tree.h"
#include <algorithm>
#include <queue>
#include <QDebug>

using namespace std;

namespace BVH
{

Comparator::Comparator(const std::vector<Face> &faces, const std::vector<aiVector3D> &vertexs)
{
    tris_center = make_shared<vector<cl_float3>>();
    tris_center->reserve(faces.size());
    for(vector<Face>::const_iterator i=faces.begin();i!=faces.end();i++)
    {
        cl_float3 center;
        center.x = (vertexs[i->a].x+vertexs[i->b].x+vertexs[i->c].x)/3;
        center.x = (vertexs[i->a].y+vertexs[i->b].y+vertexs[i->c].y)/3;
        center.x = (vertexs[i->a].z+vertexs[i->b].z+vertexs[i->c].z)/3;
        tris_center->push_back(center);
    }
}

bool Comparator::operator()(int a, int b)
{
    switch(axis)
    {
    case 0:
        return (*tris_center)[a].x < (*tris_center)[b].x;
        break;
    case 1:
        return (*tris_center)[a].y < (*tris_center)[b].y;
        break;
    case 2:
        return (*tris_center)[a].z < (*tris_center)[b].z;
        break;
    }
    return (*tris_center)[a].x < (*tris_center)[b].x;
}

void Tree::linearizeTree(std::vector<Node> &tree, const std::vector<Face> &faces, cl_uint node)
{
    Node &n = tree[node];
    linearTree.push_back(LinearNode(n));

    cl_uint ln = linearTree.size();
    if(n.isLeftLeaf())linearTree.push_back(LinearNode(faces[n.getLeft()]));
    else linearizeTree(tree, faces, n.getLeft());

    cl_uint rn = linearTree.size();
    if(n.isRightLeaf())linearTree.push_back(LinearNode(faces[n.getRight()]));
    else linearizeTree(tree, faces, n.getRight());

    for(uint i=ln;i<rn;i++)
        if((linearTree[i].nextNode&0x7fffffff)==0x7fffffff)
            linearTree[i].setNextNode(rn);

    linearTree[ln].setNextNode(rn);
}

void Tree::buildTree(const std::vector<Face> &faces, const std::vector<aiVector3D> &vertexs)
{
    vector<uint> tris_index;
    std::vector<Node> tree;

    tris_index.reserve(faces.size());
    for(uint i=0;i<faces.size();i++)
        tris_index.push_back(i);

    Comparator comp(faces, vertexs);

    queue<NodeItem> queue;
    struct NodeItem n = {0, 0, (uint)faces.size()-1, 0};
    tree.push_back(Node(0));
    queue.push(n);

    while(!queue.empty())
    {
        NodeItem n = queue.front();
        queue.pop();

        comp.axis = n.axis;
        sort(tris_index.begin()+n.start, tris_index.begin()+n.end+1, comp);
        uint mid = n.start+(n.end-n.start)/2;
        uint axis = (n.axis+1)%3;
        if(mid-n.start)
        {
            tree[n.node].setLeftNode(tree.size());
            NodeItem a = {(uint)tree.size(), n.start, mid, axis};
            tree.push_back(Node(n.node));
            queue.push(a);
        }
        else tree[n.node].setLeftTriangle(tris_index[n.start]);
        if(n.end-mid-1)
        {
            tree[n.node].setRightNode(tree.size());
            NodeItem b = {(uint)tree.size(), mid+1, n.end, axis};
            tree.push_back(Node(n.node));
            queue.push(b);
        }
        else tree[n.node].setRightTriangle(tris_index[n.end]);
    }
    for(int i=tree.size()-1;i>=0;i--)tree[i].calcAABB(faces, vertexs, tree);

    linearTree.reserve(tree.size());
    linearizeTree(tree, faces, 0);

    qDebug() << "linear" << tree.size() << linearTree.size();
    /*for(uint i=0;i<linearTree.size();i++)
    {
        //qDebug() << i << (linearTree[i].nextNode&0x7fffffff) << (linearTree[i].nextNode>>31);
        LinearNode &n = linearTree.at(i);
        if(n.nextNode&0x80000000)
            qDebug() << i << n.bvhNode.min[0] << n.bvhNode.min[0] << n.bvhNode.min[2] << "|" << n.bvhNode.max[0] << n.bvhNode.max[0] << n.bvhNode.max[2];
        else
            qDebug() << i << n.triangle.vertexs[0] << n.triangle.vertexs[1] << n.triangle.vertexs[2];
    }*/
}

Tree::Tree()
{
}

Tree::Tree(const std::vector<Face> &faces, const std::vector<aiVector3D> &vertexs)
{
    buildTree(faces, vertexs);
}

}
