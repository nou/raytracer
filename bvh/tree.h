#ifndef BVHTREE_H
#define BVHTREE_H

#include <vector>
#include <memory>
#include <CL/cl.h>
#include "node.h"
#include "assimpmodel.h"

namespace BVH
{

class Comparator
{
    std::shared_ptr<std::vector<cl_float3>> tris_center;
public:
    Comparator(const std::vector<Face> &faces, const std::vector<aiVector3D> &vertexs);
    bool operator()(int a, int b);
    uint axis;
};

struct NodeItem
{
    uint node, start, end, axis;
};

class Tree
{
    void linearizeTree(std::vector<Node> &tree, const std::vector<Face> &faces, cl_uint node);
public:
    std::vector<LinearNode> linearTree;
    void buildTree(const std::vector<Face> &faces, const std::vector<aiVector3D> &vertexs);
public:
    Tree();
    Tree(const std::vector<Face> &faces, const std::vector<aiVector3D> &vertexs);
};

}

#endif // BVHTREE_H
