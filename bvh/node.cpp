#include "node.h"
#include <cmath>

namespace BVH
{

aiVector3D min(const aiVector3D &a, const aiVector3D &b)
{
    aiVector3D ret;
    ret.x = std::fmin(a.x, b.x);
    ret.y = std::fmin(a.y, b.y);
    ret.z = std::fmin(a.z, b.z);
    return ret;
}

cl_float3 min(const aiVector3D &a, const aiVector3D &b, const aiVector3D &c)
{
    aiVector3D ret = min(min(a, b), c);
    return {ret.x, ret.y, ret.z};
}

cl_float3 min(const cl_float3 &a, const cl_float3 &b)
{
    cl_float3 ret;
    ret.x = std::fmin(a.x, b.x);
    ret.y = std::fmin(a.y, b.y);
    ret.z = std::fmin(a.z, b.z);
    return ret;
}

aiVector3D max(const aiVector3D &a, const aiVector3D &b)
{
    aiVector3D ret;
    ret.x = std::fmax(a.x, b.x);
    ret.y = std::fmax(a.y, b.y);
    ret.z = std::fmax(a.z, b.z);
    return ret;
}

cl_float3 max(const aiVector3D &a, const aiVector3D &b, const aiVector3D &c)
{
    aiVector3D ret = max(max(a, b), c);
    return {ret.x, ret.y, ret.z};
}

cl_float3 max(const cl_float3 &a, const cl_float3 &b)
{
    cl_float3 ret;
    ret.x = std::fmax(a.x, b.x);
    ret.y = std::fmax(a.y, b.y);
    ret.z = std::fmax(a.z, b.z);
    return ret;
}

Node::Node()
{
    left = right = parent = 0xffffffff;
}

Node::Node(uint p)
{
    parent = p;
}

Node::Node(uint a, uint b)
{
    left = a;
    right = b;
}

void Node::setLeftNode(uint i)
{
    left = i;
}

void Node::setRightNode(uint i)
{
    right = i;
}

void Node::setParent(uint i)
{
    parent = i;
}

void Node::setLeftTriangle(cl_uint i)
{
    left = i|0x80000000;
}

void Node::setRightTriangle(cl_uint i)
{
    right = i|0x80000000;
}

void Node::calcAABB(const std::vector<Face> &faces, const std::vector<aiVector3D> &vertexs, std::vector<Node> nodes)
{
    cl_float3 mina,minb,maxa,maxb;
    if(left&0x80000000)
    {
        const Face &f = faces.at(left&0x7fffffff);
        mina = BVH::min(vertexs[f.a], vertexs[f.b], vertexs[f.c]);
        maxa = BVH::max(vertexs[f.a], vertexs[f.b], vertexs[f.c]);
    }
    else
    {
        mina = nodes[left].min;
        maxa = nodes[left].max;
    }
    if(right&0x80000000)
    {
        const Face &f = faces.at(right&0x7fffffff);
        minb = BVH::min(vertexs[f.a], vertexs[f.b], vertexs[f.c]);
        maxb = BVH::max(vertexs[f.a], vertexs[f.b], vertexs[f.c]);
    }
    else
    {
        minb = nodes[right].min;
        maxb = nodes[right].max;
    }
    min = BVH::min(mina, minb);
    max = BVH::max(maxa, maxb);

    min.x -= aabb_epsilon;
    min.y -= aabb_epsilon;
    min.z -= aabb_epsilon;
    max.x += aabb_epsilon;
    max.y += aabb_epsilon;
    max.z += aabb_epsilon;
}

LinearNode::LinearNode(const Node &node) :
    nextNode(0xffffffff)
{
    cl_float3 m = node.getMin();
    bvhNode.min[0] = m.x;
    bvhNode.min[1] = m.y;
    bvhNode.min[2] = m.z;
    m = node.getMax();
    bvhNode.max[0] = m.x;
    bvhNode.max[1] = m.y;
    bvhNode.max[2] = m.z;
}

LinearNode::LinearNode(const Face &face) :
    nextNode(0x7fffffff)
{
    triangle.vertexs[0] = face.a;
    triangle.vertexs[1] = face.b;
    triangle.vertexs[2] = face.c;
    triangle.matIndex = face.m;
}

cl_float3 LinearNode::getMin() const
{
    cl_float3 ret = {bvhNode.min[0], bvhNode.min[1], bvhNode.min[2]};
    return ret;
}

cl_float3 LinearNode::getMax() const
{
    cl_float3 ret = {bvhNode.max[0], bvhNode.max[1], bvhNode.max[2]};
    return ret;
}

void LinearNode::setNextNode(cl_uint node)
{
    nextNode = (nextNode&0x80000000) | (node&0x7fffffff);
}

}
