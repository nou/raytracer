#ifndef MWC64X_H
#define MWC64X_H

#include <sys/types.h>
#include <CL/cl_platform.h>

typedef cl_uint2 mwc64x_state_t;

void MWC64X_Skip(mwc64x_state_t *s, cl_ulong distance);

#endif // MWC64X_H
