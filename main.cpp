#include <iostream>
#include <QApplication>
#include <QMessageBox>
#include <QCommandLineParser>
#include "mainwindow.h"

using namespace std;

int main(int argc, char **argv)
{
    if(argc>1)
    {
        QCoreApplication app(argc, argv);
        QCommandLineParser parser;
        parser.setApplicationDescription("Light Speed Raytracer\n\nThis is part of master thesis");
        parser.addHelpOption();
        parser.addOption(QCommandLineOption("p", "Index of platform to use", "platform", "0"));
        parser.addOption(QCommandLineOption("d", "Device index to use", "device", "0"));
        parser.addOption(QCommandLineOption("o", "Output dir where to save. Default is ./video", "output path", "video"));
        parser.addOption(QCommandLineOption("i", "Number of iterations. Default 10", "n", "10"));
        parser.addOption(QCommandLineOption("g", "Gain which will be appliced  to output Default is 1.0", "gain", "1.0"));
        parser.addPositionalArgument("file", "Path to file with scene config");

        parser.process(app);

        std::vector<cl::Platform> platforms;
        cl::Platform::get(&platforms);
        std::vector<cl::Device> devices;

        if(parser.value("p").toUInt()>=platforms.size())
        {
            cout << "Platform out of range. Maximum is: " << platforms.size()-1 << std::endl;
            return -1;
        }
        platforms[parser.value("p").toUInt()].getDevices(CL_DEVICE_TYPE_ALL, &devices);
        if(parser.value("d").toUInt()>=devices.size())
        {
            cout << "Device out of range. Maximum is: " << devices.size()-1 << std::endl;
            return -1;
        }

        QStringList posArgs = parser.positionalArguments();

        TracerConfig config = TracerConfig::loadFromFile(posArgs.at(0));
        if(!config.valid)
        {
            std::cout << config.error.toStdString() << std::endl;
            return -1;
        }
        try
        {
            TracerThread thread(devices[parser.value("d").toInt()]);
            thread.setScene(config);
            thread.setOutputPathGain(parser.value("o"), parser.value("g").toFloat());
            QObject::connect(&thread, SIGNAL(finished()), &app, SLOT(quit()));
            thread.start(parser.value("i").toUInt());
            return app.exec();
        }
        catch(cl::Error err)
        {
            std::cout << "Error during OpenCL execution " << err.what() << " " << err.err() << std::endl;
        }
    }
    else
    {
        QApplication app(argc, argv);
        try
        {
            app.setOrganizationName("nou");
            app.setApplicationName("raytracer");

            MainWindow m;
            m.show();

            return app.exec();
        }
        catch(cl::Error err)
        {
            QMessageBox::critical(0, QObject::tr("OpenCL error"),
                                  QObject::tr("Error during OpenCL excution %1 %2").
                                  arg(err.what()).arg(err.err()));
        }
    }
    return -1;
}
