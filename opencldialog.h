#ifndef OPENCLDIALOG_H
#define OPENCLDIALOG_H

#include <QDialog>
#include <CL/cl.hpp>

namespace Ui {
class OpenCLDialog;
}

class OpenCLDialog : public QDialog
{
    Q_OBJECT
    std::vector<cl::Platform> platforms;
    std::vector<cl::Device> devices;
    void loadSettings();
public:
    explicit OpenCLDialog(QWidget *parent = 0);
    ~OpenCLDialog();
    cl::Device& getSelectedDevice();
    void done(int r);
protected slots:
    void refreshDevices(int platformIdx);
private:
    Ui::OpenCLDialog *ui;
};

#endif // OPENCLDIALOG_H
