#ifndef _TRACER_H_NOU_
#define _TRACER_H_NOU_

#include <CL/cl.hpp>
#include <memory>
#include "assimpmodel.h"
#include "bvh/tree.h"
#include <QImage>
#include <QMutex>
#include <QWaitCondition>
#include "tracerconfig.h"

using namespace std;

class Tracer
{
    TracerConfig config;
    uint numSamples;
    unique_ptr<cl::Context> context;
    unique_ptr<cl::Program> program;
    unique_ptr<cl::Kernel> aabb_kernel, init_rays, get_image_kernel;
    unique_ptr<cl::Image2D> tex;
    unique_ptr<cl::CommandQueue> queue;
    unique_ptr<AssImpModel> mesh;
    unique_ptr<BVH::Tree> tree;
    cl::Buffer buffer_vertexs, buffer_nodes, buffer_materials, buffer_samples[2], buffer_rays, buffer_prng;
    float *frames;
    float *minDistance;
    cl_float4 *mapped;
    int mappedIdx;
    void initPRNG(uint numStates);
    cl_uint imgLength() const;
    cl_uint framesLenght() const;
    void integrate(const cl_float4 *mappedBuffer);
    void initBuffers();
    QImage getMinDistance() const;
public:
    Tracer();
    ~Tracer();
    void initWithDevice(cl::Device &device);
    void initWithSceneConfig(TracerConfig &_config);
    cl::Event traceStep();
    void integrateStepFirst();
    void integrateStepSecond();
    uint getNumSamples() const;
    QList<QImage> retriveFrames(float gain) const;
    bool saveFrames(const QString &path);
    bool loadFrames(const QString &path);
};

class TracerThread : public QObject
{
    Q_OBJECT
    QMutex mutex;
    QWaitCondition cond;
    QImage img;
    QThread *thread;
    bool quit;
    uint samplesToRun;
    unique_ptr<Tracer> tracer;
    QString outputPath;
    float outputGain;
public:
    TracerThread(cl::Device &device);
    ~TracerThread();
    void start(uint numSamples);
    void stop();
    QList<QImage> getFrames(float gain);
    bool setScene(TracerConfig &config);
    bool isRuning() const;
    bool saveFrames(const QString &path);
    bool loadFrames(const QString &path);
    void setOutputPathGain(const QString &path, float gain);
signals:
    void sampleFinished(int num);
    void finished();
private slots:
    void run();
};

#endif // _TRACER_H_NOU_
