#include "assimpmodel.h"
#include <QFile>
#include <QDebug>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

Face::Face(uint *face, uint mat, uint offset)
{
    a = face[0]+offset;
    b = face[1]+offset;
    c = face[2]+offset;
    m = mat;
}

AssImpModel::AssImpModel(const QString &path)
{
    Assimp::Importer importer;

    QByteArray file = QFile::encodeName(path);
    const aiScene *scene = importer.ReadFile(file.constData(), aiProcessPreset_TargetRealtime_MaxQuality);

    numVertices = 0;
    numFaces = 0;
    for(uint i=0;i<scene->mNumMeshes;i++)
    {
        aiMesh *mesh = scene->mMeshes[i];
        numVertices += mesh->mNumVertices;
        numFaces += mesh->mNumFaces;
    }
    vertexs.resize(numVertices);
    faces.reserve(numFaces);

    uint offset = 0;
    for(uint i=0;i<scene->mNumMeshes;i++)
    {
        aiMesh *mesh = scene->mMeshes[i];
        memcpy(&vertexs[offset], mesh->mVertices, sizeof(aiVector3D)*mesh->mNumVertices);

        for(uint o=0;o<mesh->mNumFaces;o++)
            faces.push_back(Face(mesh->mFaces[o].mIndices, mesh->mMaterialIndex, offset));

        offset += mesh->mNumVertices;
    }

    for(uint i=0;i<scene->mNumMaterials;i++)
    {
        aiColor3D color;
        aiMaterial *mat = scene->mMaterials[i];
        mat->Get(AI_MATKEY_COLOR_DIFFUSE, color);
        Material material = {color.r, color.g, color.b};
        materials.push_back(material);
    }
    qDebug() << scene->mNumMeshes;
    qDebug() << numVertices << numFaces;
    qDebug() << scene->mNumLights;
}

const std::vector<aiVector3D>& AssImpModel::getVertexs() const
{
    return vertexs;
}

const std::vector<Face>& AssImpModel::getFaces() const
{
    return faces;
}

const std::vector<Material>& AssImpModel::getMaterials() const
{
    return materials;
}
