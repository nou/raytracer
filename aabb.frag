#version 150

uniform vec4 color;

void main(void)
{
    gl_FragColor = color;
}
