#include "tracerconfig.h"
#include <QDomDocument>
#include <QFile>
#include <QDebug>
#include <QFileInfo>
#include <QDir>

cl_float3 getFloat3(const QDomElement &elem)
{
    cl_float3 ret;
    ret.x = elem.attribute("x", "0").toFloat();
    ret.y = elem.attribute("y", "0").toFloat();
    ret.z = elem.attribute("z", "0").toFloat();
    return ret;
}

TracerConfig::TracerConfig() :
    valid(false),
    traceDepth(5),
    numFrames(250),
    frameStep(0.1f),
    lightPos({0.8f, -0.8f, 0.3}),
    lightColor({1.0f, 1.0f, 1.0f}),
    cameraPos({0.0f, 0.0f, 4.0f}),
    cameraCenter({0.0f, 0.0f, 0.0f}),
    imgSize({512, 512})
{
}

TracerConfig TracerConfig::loadFromFile(const QString &path)
{
    TracerConfig config;
    config.valid = false;

    QFileInfo info(path);

    QDomDocument domDocument;
    QFile fr(path);
    if(!fr.open(QIODevice::ReadOnly))
    {
        config.error = QObject::tr("Can't open file");
    }
    else if(!domDocument.setContent(&fr, &config.error))
    {
    }
    else
    {
        QDomElement domElem = domDocument.documentElement();
        if(domElem.tagName()=="tracerScene")
        {
            config.valid = true;
            QDomElement elem = domElem.firstChildElement();

            while(!elem.isNull())
            {
                if(elem.tagName()=="imgSize")
                {
                    config.imgSize.x = elem.attribute("width", "512").toUInt();
                    config.imgSize.y = elem.attribute("height", "512").toUInt();
                }
                else if(elem.tagName()=="traceDepth")
                {
                    config.traceDepth = elem.attribute("depth", "2").toUInt();
                }
                else if(elem.tagName()=="lightPos")
                {
                    config.lightPos = getFloat3(elem);
                }
                else if(elem.tagName()=="lightColor")
                {
                    config.lightColor.x = elem.attribute("r", "1").toFloat();
                    config.lightColor.y = elem.attribute("g", "1").toFloat();
                    config.lightColor.z = elem.attribute("b", "1").toFloat();
                }
                else if(elem.tagName()=="camera")
                {
                    QDomElement pos = elem.firstChildElement("pos");
                    QDomElement look = elem.firstChildElement("center");
                    if(!pos.isNull())config.cameraPos = getFloat3(pos);
                    if(!look.isNull())config.cameraCenter = getFloat3(look);
                }
                else if(elem.tagName()=="model")
                {
                    QString modelPath = elem.text();
                    QFileInfo modelInfo(modelPath);
                    if(modelInfo.isRelative())
                    {
                        modelPath = info.dir().absoluteFilePath(elem.text());
                    }
                    config.model = modelPath;
                }
                else if(elem.tagName()=="frames")
                {
                    config.numFrames = elem.attribute("count", "250").toInt();
                    config.frameStep = elem.attribute("step", "0.1").toFloat();
                }

                elem = elem.nextSiblingElement();
            }
        }
        else
        {
            config.error = QObject::tr("Invalid file");
        }
    }

    return config;
}
