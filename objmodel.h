#ifndef _OBJMODEL_H_NOU_
#define _OBJMODEL_H_NOU_

#include <CL/cl.h>
#include <vector>

class ObjModel
{
    std::vector<cl_float3> vertexs;
    std::vector<cl_uint3> faces;
    public:
    ObjModel(const char* filename);
    ~ObjModel();
    std::vector<cl_float3>& getVertexs(){ return vertexs; }
    std::vector<cl_uint3>& getFaces(){ return faces; }
};

#endif // _OBJMODEL_H_NOU_
