#include "mwc64x.h"

/*
Part of MWC64X by David Thomas, dt10@imperial.ac.uk
This is provided under BSD, full license is with the main package.
See http://www.doc.ic.ac.uk/~dt10/research
*/
#ifndef dt10_mwc64x_rng_cl
#define dt10_mwc64x_rng_cl

// Pre: a<M, b<M
// Post: r=(a+b) mod M
cl_ulong MWC_AddMod64(cl_ulong a, cl_ulong b, cl_ulong M)
{
    cl_ulong v=a+b;
    if( (v>=M) || (v<a) )
        v=v-M;
    return v;
}

// Pre: a<M,b<M
// Post: r=(a*b) mod M
// This could be done more efficently, but it is portable, and should
// be easy to understand. It can be replaced with any of the better
// modular multiplication algorithms (for example if you know you have
// double precision available or something).
cl_ulong MWC_MulMod64(cl_ulong a, cl_ulong b, cl_ulong M)
{
    cl_ulong r=0;
    while(a!=0){
        if(a&1)
            r=MWC_AddMod64(r,b,M);
        b=MWC_AddMod64(b,b,M);
        a=a>>1;
    }
    return r;
}

// Pre: a<M, e>=0
// Post: r=(a^b) mod M
// This takes at most ~64^2 modular additions, so probably about 2^15 or so instructions on
// most architectures
cl_ulong MWC_PowMod64(cl_ulong a, cl_ulong e, cl_ulong M)
{
    cl_ulong sqr=a, acc=1;
    while(e!=0){
        if(e&1)
            acc=MWC_MulMod64(acc,sqr,M);
        sqr=MWC_MulMod64(sqr,sqr,M);
        e=e>>1;
    }
    return acc;
}

cl_uint2 MWC_SkipImpl_Mod64(cl_uint2 curr, cl_ulong A, cl_ulong M, cl_ulong distance)
{
    cl_ulong m=MWC_PowMod64(A, distance, M);
    cl_ulong x=curr.x*(cl_ulong)A+curr.y;
    x=MWC_MulMod64(x, m, M);
    return (cl_uint2){(cl_uint)(x/A), (cl_uint)(x%A)};
}

enum{ MWC64X_A = 4294883355U };
enum{ MWC64X_M = 18446383549859758079UL };

void MWC64X_Skip(mwc64x_state_t *s, cl_ulong distance)
{
    cl_uint2 tmp = MWC_SkipImpl_Mod64(*s, MWC64X_A, MWC64X_M, distance);
    s->x=tmp.x;
    s->y=tmp.y;
}

#endif
