#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <memory>
#include <QMainWindow>
#include "opencldialog.h"
#include "tracer.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    Ui::MainWindow *ui;
    OpenCLDialog *openclDialog;
    std::unique_ptr<TracerThread> tracerThread;
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
public slots:
    void openclSettings();
    void openScene();
    void start();
    void sampleCounter(int samples);
    void tracerStopped();
    void retriveFrames();
    void nextFrame();
    void prevFrame();
    void saveFrames();
    void loadFrames();
    void exportFrames();
};

#endif // MAINWINDOW_H
