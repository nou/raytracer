#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    openclDialog = new OpenCLDialog(this);
    connect(ui->actionOpenCL_device, SIGNAL(triggered()), this, SLOT(openclSettings()));
    connect(ui->actionOpen_scene, SIGNAL(triggered()), this, SLOT(openScene()));
    connect(ui->actionSave_frames, SIGNAL(triggered()), this, SLOT(saveFrames()));
    connect(ui->actionLoad_frames, SIGNAL(triggered()), this, SLOT(loadFrames()));
    connect(ui->actionExport_frames, SIGNAL(triggered()), this, SLOT(exportFrames()));

    tracerThread = std::unique_ptr<TracerThread>(new TracerThread(openclDialog->getSelectedDevice()));
    connect(tracerThread.get(), SIGNAL(finished()), this, SLOT(tracerStopped()));
    connect(tracerThread.get(), SIGNAL(sampleFinished(int)), this, SLOT(sampleCounter(int)));

    TracerConfig config = TracerConfig::loadFromFile("tracer_config.xml");
    tracerThread->setScene(config);

    connect(ui->startButton, SIGNAL(clicked()), this, SLOT(start()));
    connect(ui->getFramesButton, SIGNAL(clicked()), this, SLOT(retriveFrames()));
    connect(ui->prevButton, SIGNAL(clicked()), this, SLOT(prevFrame()));
    connect(ui->nextButton, SIGNAL(clicked()), this, SLOT(nextFrame()));
    connect(ui->imageSlider, SIGNAL(valueChanged(int)), ui->imgView, SLOT(showFrame(int)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::openclSettings()
{
    if(openclDialog->exec()==QDialog::Accepted)
    {
        QMessageBox::information(this, tr("Restart application"), tr("To take effect you must restart the application"));
        //tracerThread->setDevice(openclDialog->set);
    }
}

void MainWindow::openScene()
{
    QString path = QFileDialog::getOpenFileName(this, tr("Open scene description"));
    if(!path.isEmpty())
    {
        TracerConfig config = TracerConfig::loadFromFile(path);
        if(config.valid)
        {
            tracerThread->setScene(config);
        }
        else
        {
            QMessageBox::warning(this, tr("Error during opening"), config.error);
        }
    }
}

void MainWindow::start()
{
    if(tracerThread->isRuning())
    {
        tracerThread->stop();
        //ui->startButton->setText(tr("Stop"));
    }
    else
    {
        tracerThread->start(ui->samplesSpinBox->value());
        ui->startButton->setText(tr("Stop"));
    }
}

void MainWindow::sampleCounter(int samples)
{
    ui->sampleCounterLabel->setText(QString("Samples: %1").arg(samples));
}

void MainWindow::tracerStopped()
{
    ui->startButton->setText(tr("Start"));
}

void MainWindow::retriveFrames()
{
    if(!tracerThread->isRuning())
    {
        ui->imgView->setFrames(tracerThread->getFrames(ui->gainSpinBox->value()));
        ui->imageSlider->setMaximum(ui->imgView->numFrames());
    }
    else
    {
        QMessageBox::warning(this, tr("Can't retrive frames"), tr("Can't retrive frames when simulation is running!\n"
                                                                  "Stop first simulation"));
    }
}

void MainWindow::nextFrame()
{
    ui->imageSlider->setValue(ui->imageSlider->value()+1);
}

void MainWindow::prevFrame()
{
    ui->imageSlider->setValue(ui->imageSlider->value()-1);
}

void MainWindow::saveFrames()
{
    QString path = QFileDialog::getSaveFileName(this, tr("Save frames"));
    if(!path.isEmpty())
        tracerThread->saveFrames(path);
}

void MainWindow::loadFrames()
{
    QString path = QFileDialog::getOpenFileName(this, tr("Load frames"));
    if(!path.isEmpty())
        tracerThread->loadFrames(path);
}

void MainWindow::exportFrames()
{
    QString path = QFileDialog::getExistingDirectory(this, tr("Export frames"));
    if(!path.isEmpty())
        ui->imgView->saveFrames(path);
}
