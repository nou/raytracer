#ifndef TRACERCONFIG_H
#define TRACERCONFIG_H

#include <QString>
#include <CL/cl.h>

struct TracerConfig
{
    bool valid;
    QString error;
    cl_int traceDepth;
    cl_int numFrames;
    cl_float frameStep;
    cl_float3 lightPos;
    cl_float3 lightColor;
    cl_float3 cameraPos;
    cl_float3 cameraCenter;
    cl_uint2 imgSize;
    QString model;

    TracerConfig();
    static TracerConfig loadFromFile(const QString &path);
};

#endif // TRACERCONFIG_H
