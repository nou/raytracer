#version 150

in vec3 min;
in vec3 max;

out vert_geom
{
    vec4 min;
    vec4 max;
}_out;

void main()
{
    _out.min = vec4(min, 1.0);
    _out.max = vec4(max, 1.0);
}
