typedef struct
{
    union
    {
        struct
        {
            float min[3];
            float max[3];
        }bvhNode;
        struct
        {
            uint vertexs[3];
            uint matIndex;
        }triangle;
    };
    uint nextNode;
}AABBNode;

typedef struct
{
    float3 intesity;
    float3 position;
}Light;

typedef struct
{
    float3 color;
}Material;

typedef struct
{
    float3 origin;
    float3 dir;
}Ray;

typedef struct
{
    Ray ray;
    float3 accumColor;
}RayState;

#define LINEARIZE(d, size) (mad24(d.y, size.x, d.x))
#define LINEARIZES(d, size, offset) (mad24(d.y, size.x, d.x)+offset)

__constant const float startOffset = 0.0001f;

float3 baricentric_interpolate(float3 a, float3 b, float3 c, float2 uv)
{
    return (1-uv.x-uv.y)*a + uv.x*b + uv.y*c;
}

bool aabb_hit(Ray ray, const __global AABBNode *node)
{
    float3 bbMin = vload3(0, node->bvhNode.min);
    float3 bbMax = vload3(0, node->bvhNode.max);
    float3 t1 = (bbMin - ray.origin)/ray.dir;
    float3 t2 = (bbMax - ray.origin)/ray.dir;

    float3 tmin = min(t1, t2);
    float3 tmax = max(t1, t2);

    float near = max(max(tmin.x, tmin.y), tmin.z);
    float far = min(min(tmax.x, tmax.y), tmax.z);

    return near<=far && far >= 0;
}

//vracia t,u,v. origin+d*t=hit point, u,v=barycentricke koordinaty
float3 triangle_hit(Ray ray, float3 a, float3 b, float3 c)
{
    float3 e1 = b-a;
    float3 e2 = c-a;
    float3 T = ray.origin-a;

    float3 p = cross(ray.dir, e2);
    float3 q = cross(T, e1);

    float r = 1.0f/dot(p, e1);
    float t = r*dot(q, e2);
    float u = r*dot(p, T);
    float v = r*dot(q, ray.dir);
    return (float3)(t, u, v);
}

float3 sRGB(float3 in)
{
    return select(1.055f*pow(in, 1/2.4f)-0.055f,
        12.92f*in,
        islessequal(in, (float3)(0.0031308f)));
}

float3 lambert(float3 pos, float3 normal, const Light *light, __constant Material *material)
{
    float3 l = light->position-pos;
    float squaredDistance = 1.0f/dot(l, l);
    float3 ret = material->color*light->intesity*max(dot(normalize(l), normal), 0.0f)*squaredDistance;
    return ret;
}

uint mwci(uint2 *state)
{
    enum { A=4294883355U };
    uint x=(*state).x, c=(*state).y;  // Unpack the state
    uint res=x^c;                     // Calculate the result
    uint hi=mul_hi(x,A);              // Step the RNG
    x=x*A+c;
    c=hi+(x<c);
    *state=(uint2)(x,c);              // Pack the state back up
    return res;                       // Return the next result
}

float mwcf(uint2 *state)
{
    return mwci(state)/4294967295.0f;
}

float3 mwc3f(uint2 *state)
{
    return (float3)(mwcf(state), mwcf(state), mwcf(state));
}

float3 sampleHemisphere(float r1, float r2)
{
    float3 ret;
    r1 *= 2*M_PI_F;
    ret.z = r2;
    r2 = sqrt(1-r2*r2);
    ret.x = cos(r1)*r2;
    ret.y = sin(r1)*r2;
    return ret;
}

float3 sampleHemisphereCos(float r1, float r2)
{
    float3 ret;
    r1 *= 2*M_PI_F;
    float r2s = sqrt(r2);
    ret.x = cos(r1)*r2s;
    ret.y = sin(r1)*r2s;
    ret.z = sqrt(1-r2);
    return ret;
}

float intersect(Ray ray, __global float *vertexs, __global AABBNode *nodes, int numNodes, int *triangle)
{
    uint n = 0;
    float min_t = INFINITY;
    while(n<numNodes)
    {
        __global AABBNode *node = nodes+n;
        if(node->nextNode&0x80000000)
        {
            if(aabb_hit(ray, node))n++;
            else n = node->nextNode&0x7fffffff;
        }
        else
        {
            __global uint *v = node->triangle.vertexs;
            float3 a = vload3(v[0], vertexs);
            float3 b = vload3(v[1], vertexs);
            float3 c = vload3(v[2], vertexs);

            float3 tuv = triangle_hit(ray, a, b, c);
            if(tuv.y>=0 && tuv.y<=1 && tuv.z>=0 && tuv.z<=1 && tuv.y+tuv.z<=1 && tuv.x>=0 && min_t > tuv.x)
            {
                min_t = tuv.x;
                *triangle = n;
            }
            n++;
        }
    }
    return min_t;
}

bool intersectP(Ray ray, __global float *vertexs, __global AABBNode *nodes, int numNodes)
{
    uint n = 0;
    while(n<numNodes)
    {
        __global AABBNode *node = nodes+n;
        if(node->nextNode&0x80000000)
        {
            if(aabb_hit(ray, node))n++;
            else n = node->nextNode&0x7fffffff;
        }
        else
        {
            __global uint *v = node->triangle.vertexs;
            float3 a = vload3(v[0], vertexs);
            float3 b = vload3(v[1], vertexs);
            float3 c = vload3(v[2], vertexs);

            float3 tuv = triangle_hit(ray, a, b, c);
            float len = length(ray.dir);
            tuv.x *= len;
            if(tuv.y>=0 && tuv.y<=1 && tuv.z>=0 && tuv.z<=1 && tuv.y+tuv.z<=1 && tuv.x>=0.0001f && tuv.x<len)
                return true;

            n++;
        }
    }
    return false;
}

__kernel void aabb_traverse(uint2 numNodes_Depth, __global AABBNode *restrict nodes, __global float *restrict vertexs,
                            __global Ray *restrict rays, __global float4 *restrict samples, __global uint2 *restrict rnd_states,
                            Light light, __constant Material *restrict materials)
{
    int2 gid = (int2)(get_global_id(0), get_global_id(1));
    int2 gsize = (int2)(get_global_size(0), get_global_size(1));
    int imgSize = gsize.x*gsize.y;

    Ray ray = rays[LINEARIZE(gid, gsize)];
    uint2 rnd_state = rnd_states[LINEARIZE(gid, gsize)];

    int triangle = -1;
    float3 hit;
    float3 color = (float3)(0);
    float3 accuColor = (float3)(1);
    float len = 0;

    for(int i=0;i<numNodes_Depth.y;i++)
    {
        float t = intersect(ray, vertexs, nodes, numNodes_Depth.x, &triangle);

        if(isfinite(t))
        {
            Ray shadowRay;
            hit = t*ray.dir+ray.origin;
            shadowRay.dir = light.position - hit;
            shadowRay.origin = hit;

            __global uint *v = nodes[triangle].triangle.vertexs;
            float3 a = vload3(v[0], vertexs);
            float3 b = vload3(v[1], vertexs);
            float3 c = vload3(v[2], vertexs);
            float3 normal = normalize(cross(b-a, c-a));

            normal *= dot(-ray.dir, normal)<0 ? -1 : 1;

            len += length(hit-ray.origin);

            __constant Material *material = materials+nodes[triangle].triangle.matIndex;

            float3 Ld = lambert(hit, normal, &light, material);
            bool s = intersectP(shadowRay, vertexs, nodes, numNodes_Depth.x);
            if(s)Ld = (float3)(0);
            color = Ld*accuColor;
            accuColor *= material->color;

            float3 hemi = sampleHemisphereCos(mwcf(&rnd_state), mwcf(&rnd_state));
            float3 wu = cross(normal, fabs(normal.x)>0.1f ? (float3)(0, 1, 0) : (float3)(1, 0, 0));
            float3 wv = cross(normal, wu);
            ray.dir = hemi.x*wu + hemi.y*wv + hemi.z*normal;
            float3 offset = ray.dir*startOffset;
            ray.origin = hit + offset;
            len += length(offset);

            samples[LINEARIZES(gid, gsize, imgSize*i)] = (float4)(color, len+length(shadowRay.dir));
        }
        else
        {
            samples[LINEARIZES(gid, gsize, imgSize*i)] = (float4)(-1);
            break;
        }
    }
    //samples[LINEARIZE(gid, gsize)] += (float4)(color, 0.0f);
    rnd_states[LINEARIZE(gid, gsize)] = rnd_state;
}

__kernel void init_rays(__global Ray *rays, __global uint2 *rnd_states, float16 viewMat)
{
    int2 gid = (int2)(get_global_id(0), get_global_id(1));
    int2 gsize = (int2)(get_global_size(0), get_global_size(1));

    float recp_h = 1.0f/gsize.y;
    uint2 rnd_state = rnd_states[LINEARIZE(gid, gsize)];
    float2 jitter = (float2)(mwcf(&rnd_state), mwcf(&rnd_state));

    float4 view = (float4)((gid.x-0.5f*gsize.x+jitter.x)*recp_h,
                           (gid.y+jitter.y)*recp_h - 0.5f,
                           1.5f, 0.0f);

    view = viewMat.lo.lo*view.x + viewMat.lo.hi*view.y + viewMat.hi.lo*view.z;

    rays[LINEARIZE(gid, gsize)].origin = viewMat.sCDE;
    rays[LINEARIZE(gid, gsize)].dir = view.xyz;
    rnd_states[LINEARIZE(gid, gsize)] = rnd_state;
}

__kernel void get_image(__global float3 *samples, __write_only image2d_t output, uint numSamples)
{
    int2 gid = (int2)(get_global_id(0), get_global_id(1));
    int2 gsize = (int2)(get_global_size(0), get_global_size(1));

    float3 rgb = sRGB(samples[LINEARIZE(gid, gsize)]/numSamples);
    write_imagef(output, gid, (float4)(rgb, 1));
}
