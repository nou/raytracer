#include "opencldialog.h"
#include "ui_opencldialog.h"
#include <QSettings>

void OpenCLDialog::loadSettings()
{
    QSettings settings;
    settings.beginGroup("opencl");
    ui->platformComboBox->setCurrentIndex(settings.value("platform", 0).toInt());
    ui->deviceComboBox->setCurrentIndex(settings.value("device", 0).toInt());
}

OpenCLDialog::OpenCLDialog(QWidget *parent) : QDialog(parent),
    ui(new Ui::OpenCLDialog)
{
    ui->setupUi(this);
    connect(ui->platformComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(refreshDevices(int)));

    cl::Platform::get(&platforms);

    for(auto &platform : platforms)
    {
        std::string name = platform.getInfo<CL_PLATFORM_NAME>();
        ui->platformComboBox->addItem(name.c_str());
    }

    loadSettings();
}

OpenCLDialog::~OpenCLDialog()
{
    delete ui;
}

cl::Device& OpenCLDialog::getSelectedDevice()
{
    return devices[ui->deviceComboBox->currentIndex()];
}

void OpenCLDialog::done(int r)
{
    if(r==QDialog::Accepted)
    {
        QSettings settings;
        settings.beginGroup("opencl");
        settings.setValue("platform", ui->platformComboBox->currentIndex());
        settings.setValue("device", ui->deviceComboBox->currentIndex());
    }
    QDialog::done(r);
}

void OpenCLDialog::refreshDevices(int platformIdx)
{
    platforms[platformIdx].getDevices(CL_DEVICE_TYPE_ALL, &devices);

    ui->deviceComboBox->clear();

    for(auto &device : devices)
    {
        std::string name = device.getInfo<CL_DEVICE_NAME>();
        ui->deviceComboBox->addItem(name.c_str());
    }
}
