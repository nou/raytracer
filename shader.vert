#version 150

in vec4 vertex;
uniform mat4 ModelViewProjectionMatrix;

void main(void)
{
    gl_Position =  ModelViewProjectionMatrix * vertex;
}
